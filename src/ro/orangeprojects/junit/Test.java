package ro.orangeprojects.junit;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {



        List<Item> item = new ArrayList<Item>();
        item.add(new Item("towel", 3));
        item.add(new Item("sheets", 5));

        int quantity = item.stream()
                .mapToInt(i  -> i.getQuantity()

           )
                .sum();

        System.out.println(quantity);


    }
}
