package ro.orangeprojects.junit;

public class Item {
    private String name;
    private int quantity;

    public int getQuantity() {
        return quantity;
    }

    public String getName() {
        return name;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Item (String name, int quantity){
        this.name = name;
        this.quantity = quantity;
    }
}
